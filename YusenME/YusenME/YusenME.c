/*********************************************************************************
* YusenME.c -  message exit to modify received messages to prevent security
*              problems when using COA, COD, Expiry messages by modifying the
*              received userid.
*              The exit can used to change the reply to queue manager in sent or
*              received messages and set it to a value that suits your
*              installation to increase Security.
*              For usage see Redbook SG-xxxx-xx available from:
*              http://www.redbooks.ibm.com/
*
* -------------------------------------------------------------------------------
* Description
*
* This message exit is designed to remove/replace the received userid with the
* MCAUSER defined on the channel definition to prevent security violations when
* forwarding message thru an intermediate queue manager.
*
* The exit needs to be added to the channel definitions on the RCVR/RQSTR
* channel.
*
* It is based on itsoME from the Secure Messaging Scenarios with WebSphere MQ
* redbook publication, and customized for Yusen Logistics Australia by
* Syntegrity Solutions.
*
* August 2017:
* Due to changes in Windows safe string handing, the cross
* compilability of the code has been removed.
*
* Safe string handling and safe handling of 64 bit pointers
* has been added
*
* Compilation:
* Windows:
*   cl /LD  YusenME.c advapi32.lib 
*
* YusenME.exp contains:
MsgExit
MQStart
*
* -------------------------------------------------------------------------------
* History
*
* Written by J�rgen Pedersen                           - 01-Jul12 DK  v1.00
* email: jopde@dk.ibm.com
* Modified by Neil Casey                               - 01-Aug17 NC  v1.03
* email: Neil.Casey@syntegrity.com.au
*
* -------------------------------------------------------------------------------
* Documentation
*
*
* -------------------------------------------------------------------------------
* Channel definitions
*
* To add msgexit to channel definitions distributed queue managers:
*   alt chl(MQT2.TCP.MQT1) chltype(RCVR) +      * OR: RQSTR/SDR/SVR
*   msgexit('YusenME(MsgExit)') +               *
*   msgdata('MCA/NONE/')                        *
*
* Usage:
* syntax: MSGDATA('Option/[Option/][Option/]')
* options:
* MCA   - Setting received UserIdentifier in Message Decscriptor to the
*         value of MCAUser field from the channel
* COA   - removes the COA and COD report options
* EXP   - removes Exception and Expiery report options
* DISC  - removes discard options preventing snooping. Msgs will go into DLQ
* ALL   - clears COA/EXP/DISC options off
* NONE  - clears COA/EXP/DISC options off
* RTQM= - Prefix ReplyToQueue with ReplyToQmgr'.'
*
* Example to remove COA and set UserIdentifer to MCA value:
* msgdata('COA/MCA/')
* Remember the trailing slash '/' otherwise the option will be ignored.
*/

/*********************************************************************************
* -- environment --
********************************************************************************/

/* define to get debug message output, either below or in compile cmd line      */
#if !defined(DEBUGMSG)
#define DEBUGMSG   0                 /* 0 - no output, 1 - stdout, 2 - file out */
#endif

#define SRCID "@(#)YusenME.c 1.03 DATE01-Aug-2017"
#define CPRID "@(#)Copyright 2012 IBM"
#define OWNID "@(#)Licensed Materials - Property of IBM"

/*********************************************************************************
* -- Includes --
********************************************************************************/

/* standard headers                                                             */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* MQ headers                                                                   */
#include <cmqc.h>
#include <cmqxc.h>

#include <Windows.h>
/*********************************************************************************
* -- defines & macros --
********************************************************************************/

#define EXPORT __declspec(dllexport)    /* exported from DLL */

/* Options for controlling operation                                            */
#define MQME_NONE  0x0000000000000000
#define MQME_ALL   0x0000000000000007
#define MQME_COA   0x0000000000000001
#define MQME_EXP   0x0000000000000002
#define MQME_DISC  0x0000000000000004
#define MQME_MCA   0x0000000000001000
#define MQME_RTQM  0x0000000000002000

#define VERSION      "1.03"                          /* Version no              */

#define MSGFILE   "c:\\MQ\\YUSENPWE.log" /* <-- tailor for environment */

/*********************************************************************************
*
* debug message generation
*
********************************************************************************/

#if DEBUGMSG == 1
/* debug messages to stdout */
#define OST stdout
#define DBGPRINTF(x) fprintf_s x
#elif DEBUGMSG == 2

FILE * OST = 0;
#define DBGPRINTF(x) { \
                     fopen_s(&OST, MSGFILE, "a+"); \
                     if(OST) fprintf_s x; \
                     fclose(OST); \
                     }

#else
/* no debug messages */
#define DBGPRINTF(x)
#endif /* DEBUGMSG */

/* Macro used for logging hello, bye and errors                                 */
#define DBGLOG(x) { \
                     OSL = trcbuf ; \
                     sprintf_s x ; \
                     SendLogMessage(OSL); \
                     }

void SendLogMessage(char *szData);

/*********************************************************************************
* rlnts()
*
* Remove trailing spaces
*
*
* @return length, string has no trailing spaces
********************************************************************************/
static int rlnts(int l, char * s)
{
	while (l && s[l - 1] == ' ') --l;
	return l;
}


static char SrcIdent[] = SRCID, CprIdent[] = CPRID, OwnIdent[] = OWNID;

void strlower(char* string, int string_length)
{
	size_t length = string_length < 0 ? strlen(string) : string_length;
	while (length--)
	{
		*string = tolower(*string);
		string++;
	}
}

void strupper(char* string, int string_length)
{
	size_t length = string_length < 0 ? strlen(string) : string_length;
	while (length--)
	{
		*string = toupper(*string);
		string++;
	}
}

/*********************************************************************************
* MQExitReason()
*
* Return ExitReasonText string
*
*
* @return exit Reason text.
********************************************************************************/
char * MQExitReason(PMQCXP pChannelExitParams)
{
	switch (pChannelExitParams->ExitReason)
	{
	case(MQXR_INIT):            return("MQXR_INIT");
	case(MQXR_TERM):            return("MQXR_TERM");
	case(MQXR_MSG):             return("MQRX_MSG");
	case(MQXR_XMIT):            return("MQXR_XMIT");
	case(MQXR_SEC_MSG):         return("MQXR_SEC_MSG");
	case(MQXR_INIT_SEC):        return("MQXR_INIT_SEC");
	case(MQXR_RETRY):           return("MQXR_RETRY");
	case(MQXR_AUTO_CLUSSDR):    return("MQXR_AUTO_CLUSSDR");
	case(MQXR_AUTO_RECEIVER):   return("MQXR_AUTO_RECEIVER");
	case(MQXR_AUTO_SVRCONN):    return("MQXR_AUTO_SVRCONN");
	case(MQXR_AUTO_CLUSRCVR):   return("MQXR_AUTO_CLUSRCVR");
#ifdef MQXR_SEC_PARMS
	case(MQXR_SEC_PARMS):         return("MQXR_SEC_PARMS");
#endif
	default:                    return("Unknown ExitReason");
	}                                                /* end switch              */
}

/*********************************************************************************
* MQExitId()
*
* Return ExitIdText string
*
*
* @return exit Id text.
********************************************************************************/
char * MQExitId(PMQCXP pChannelExitParams)
{
	switch (pChannelExitParams->ExitId)
	{
	case(MQXT_CHANNEL_SEC_EXIT):         return("MQXT_CHANNEL_SEC_EXIT");
	case(MQXT_CHANNEL_MSG_EXIT):         return("MQXT_CHANNEL_MSG_EXIT");
	case(MQXT_CHANNEL_SEND_EXIT):        return("MQXT_CHANNEL_SEND_EXIT");
	case(MQXT_CHANNEL_RCV_EXIT):         return("MQXT_CHANNEL_RCV_EXIT");
	case(MQXT_CHANNEL_MSG_RETRY_EXIT):   return("MQXT_CHANNEL_MSG_RETRY_EXIT");
	case(MQXT_CHANNEL_AUTO_DEF_EXIT):    return("MQXT_CHANNEL_AUTO_DEF_EXIT");
	default:                             return("Unknown ExitID");
	}                                                /* end switch              */
}

/*********************************************************************************
* MQChannelType()
*
* Return ChannelTypeText string
*
*
* @return exit ChannelType text.
********************************************************************************/
char * MQChannelType(PMQCD pChannelDefinition)
{
	switch (pChannelDefinition->ChannelType)
	{
	case(MQCHT_SENDER):         return("MQCHT_SENDER");
	case(MQCHT_SERVER):         return("MQCHT_SERVER");
	case(MQCHT_RECEIVER):       return("MQCHT_RECEIVER");
	case(MQCHT_REQUESTER):      return("MQCHT_REQUESTER");
	case(MQCHT_CLNTCONN):       return("MQCHT_CLNTCONN");
	case(MQCHT_SVRCONN):        return("MQCHT_SVRCONN");
	case(MQCHT_CLUSSDR):        return("MQCHT_CLUSSDR");
	case(MQCHT_CLUSRCVR):       return("MQCHT_CLUSRCVR");
	default:                    return("Unknown ChannelType");
	}                                                /* end switch              */
}

/*********************************************************************************
* MQExitResponseType()
*
* Return ExitResponseText string
*
*
* @return exit Response text.
********************************************************************************/
char * MQExitResponseType(PMQCXP pChannelExitParams)
{
	switch (pChannelExitParams->ExitResponse)
	{
	case(MQXCC_OK):                       return("MQXCC_OK");
	case(MQXCC_SUPPRESS_FUNCTION):        return("MQXCC_SUPPRESS_FUNCTION");
	case(MQXCC_SKIP_FUNCTION):            return("MQXCC_SKIP_FUNCTION");
	case(MQXCC_SEND_AND_REQUEST_SEC_MSG): return("MQXCC_SEND_AND_REQUEST_SEC_MSG");
	case(MQXCC_SEND_SEC_MSG):             return("MQXCC_SEND_SEC_MSG");
	case(MQXCC_SUPPRESS_EXIT):            return("MQXCC_SUPPRESS_EXIT");
	case(MQXCC_CLOSE_CHANNEL):            return("MQXCC_CLOSE_CHANNEL");
	case(MQXCC_FAILED):                   return("MQXCC_FAILED");
	default:                              return("Unknown ExitResponse");
	}                                                /* end switch              */
}

/*********************************************************************************
* SendLogMessage()
*
*
* Method to log data to EventList on windows
*
*	 szData ASCII data to report
*
* @return void
*
********************************************************************************/
void SendLogMessage(char *szData)
{
	HANDLE  hSource;
	char    *szList[1];

	szList[0] = szData;
	hSource = RegisterEventSource(NULL, "MQ YusenME");
	if (hSource != NULL)
	{
		ReportEvent(hSource, EVENTLOG_INFORMATION_TYPE,
			0, 1, NULL, 1, 0, (const unsigned char**)szList, NULL);
		DeregisterEventSource(hSource);
	}
}

/********************************************************************************/

extern void MQENTRY MQStart(void) { ; }

/*********************************************************************************
* MsgExit - Channel message exit, entry point
*
* PARAMETERS:
*    pChannelExitParams (PMQCXP)  - input/output
*       Channel exit parameter block
*
*    pChannelDefinition (PMQCD)   - input/output
*       Channel definition
*
*    pDataLength        (PMQLONG) - input/output
*       On input:  length of AgentBuffer
*       On output: length of returned data (AgentBuff/ErrorBuff)
*
*    pAgentBufferLength (PMQLONG) - input
*       Agent Buffer length
*
*    AgentBuffer        (PMQBYTE) - input/output
*       Agent buffer
*
*    pExitBufferLength  (PMQLONG) - input/output
*       Exit buffer length
*
*    pExitBufferAddr    (PMQPTR)  - input/output
*       Exit buffer
*
* RETURN:
*    void
*
* NOTES:
*    pExitBufferAddr
*       This pointer is used to hold switches for communication between various
*       exit points. This was done for simplexity of this exit to provide a
*       broader solution that will work on most platforms.
*
********************************************************************************/
void EXPORT MsgExit(PMQCXP     pChannelExitParams,
	PMQCD      pChannelDefinition,
	PMQLONG    pDataLength,
	PMQLONG    pAgentBufferLength,
	PMQBYTE    AgentBuffer,
	PMQLONG    pExitBufferLength,
	PMQPTR     pExitBufferAddr)
{
	MQXQH * pXqh = (MQXQH *)AgentBuffer;

	MQCHAR  cExitDataWork[MQ_EXIT_DATA_LENGTH + 1 + 5];
	MQCHAR  cWorkRQmgr[MQ_Q_MGR_NAME_LENGTH + 1 + 5];
	MQCHAR  cWorkRQmgr1[MQ_Q_MGR_NAME_LENGTH + 1 + 5];
	MQCHAR  cWorkReplyQueue[MQ_Q_MGR_NAME_LENGTH + MQ_Q_NAME_LENGTH + 2];
	PMQCHAR p, u;
	MQINT64  lOptions = (MQINT64)MQME_NONE;
	char   *strtok_cb;

#define TRC_BUF_SIZE 255
	unsigned char trcbuf[TRC_BUF_SIZE];
	unsigned char * OSL;

	/* keep compiler quiet                                                      */
	pExitBufferAddr = pExitBufferAddr;
	pExitBufferLength = pExitBufferLength;

	/* debug info                                                               */
	DBGPRINTF((OST, "YusenME: ver=%s ExitId=%s (%ld) ExitReason=%s (%ld) "
		"ChannelType=%s (%d)\n",
		VERSION,
		MQExitId(pChannelExitParams),
		pChannelExitParams->ExitId,
		MQExitReason(pChannelExitParams),
		pChannelExitParams->ExitReason,
		MQChannelType(pChannelDefinition),
		pChannelDefinition->ChannelType));

	DBGPRINTF((OST, "YusenME: QMgrName=\"%.*s\"\n",
		rlnts(MQ_Q_MGR_NAME_LENGTH, pChannelDefinition->QMgrName),
		pChannelDefinition->QMgrName));

	DBGPRINTF((OST, "YusenME: ChannelName=\"%.*s\"\n",
		rlnts(MQ_CHANNEL_NAME_LENGTH, pChannelDefinition->ChannelName),
		pChannelDefinition->ChannelName));

	/* set default exit responses                                               */
	pChannelExitParams->ExitResponse = MQXCC_OK;
	pChannelExitParams->ExitResponse2 = MQXR2_USE_AGENT_BUFFER;

	/* ensure we are only being called as a channel msgexit                     */
	if (pChannelExitParams->ExitId != MQXT_CHANNEL_MSG_EXIT)
	{ /* error - not being invoked from msgexit */
		DBGPRINTF((OST, "YusenME: error - exit called in wrong context\n\n"));

		/* prevent exit being called again                                      */
		pChannelExitParams->ExitResponse = MQXCC_SUPPRESS_EXIT;
		return;
	}

	/* switch on ExitReason to see why exit was invoked                         */
	switch (pChannelExitParams->ExitReason)
	{

	case MQXR_INIT: /* Initialization */
		DBGLOG((OSL,
			TRC_BUF_SIZE,
			"YusenME: ver=%s Channel=\"%.*s\" started with options=\"%.*s\"\n",
			VERSION,
			rlnts(MQ_CHANNEL_NAME_LENGTH, pChannelDefinition->ChannelName),
			pChannelDefinition->ChannelName,
			rlnts(MQ_EXIT_DATA_LENGTH, pChannelDefinition->MsgUserData),
			pChannelDefinition->MsgUserData));

		DBGPRINTF((OST, "YusenME: init\n"));

		/* No options selected yet. Do nothing is default                       */
		/* NOTE: pExitBufferAddr is used to pass flags between exit points      */
		*pExitBufferAddr = (PMQPTR)MQME_NONE;

		/* prepare for parsing MsgUserData */
		memset(cExitDataWork, '\00', sizeof(cExitDataWork));
		memcpy(cExitDataWork, pChannelDefinition->MsgUserData,
			rlnts(MQ_EXIT_DATA_LENGTH, pChannelDefinition->MsgUserData));

		/* Check for trailing "/"                                               */
		if (cExitDataWork[strlen(cExitDataWork) - 1] != '/')
		{                                        /* option, full-stop       */
			DBGLOG((OSL,
				TRC_BUF_SIZE,
				"YusenME: Trailing / is missing in MSGDATA. Channel is stopped.\n"));
			pChannelExitParams->ExitResponse = MQXCC_CLOSE_CHANNEL;
			pChannelExitParams->ExitResponse2 = MQXR2_USE_AGENT_BUFFER;
			break;
		}

		/* processing the MsgUserData to find wanted options                    */
		p = (char*)strtok_s(cExitDataWork, "/", &strtok_cb);
		while (p != NULL)
		{
			if (!strncmp(p, "ALL", 3))
				lOptions = lOptions | MQME_ALL;
			else if (!strncmp(p, "COA", 3))
				lOptions = lOptions | MQME_COA;
			else if (!strncmp(p, "DLQ", 3))
				lOptions = lOptions | MQME_MCA;
			else if (!strncmp(p, "EXP", 3))
				lOptions = lOptions | MQME_EXP;
			else if (!strncmp(p, "MCA", 3))
				lOptions = lOptions | MQME_MCA;
			else if (!strncmp(p, "NONE", 4))
				lOptions = lOptions | MQME_ALL;
			else if (!memcmp(p, "RTQM=", 5))           /* we need to modify       */
				lOptions = lOptions | MQME_RTQM;     /*  reply to queue         */
			else                                     /* we have a non supported */
			{                                        /* option, full-stop       */
				DBGLOG((OSL,
					TRC_BUF_SIZE,
					"YusenME: Invalid option was specified=\"%s\" Channel is stopped.\n",
					p));
				pChannelExitParams->ExitResponse = MQXCC_CLOSE_CHANNEL;
				pChannelExitParams->ExitResponse2 = MQXR2_USE_AGENT_BUFFER;
			}

			p = (char*)strtok_s(NULL, "/", &strtok_cb);  /* look for next token */
		}

		/* NOTE: pExitBufferAddr is used to pass flags between exit points      */
		*pExitBufferAddr = (PMQPTR)lOptions;
		DBGPRINTF((OST, "YusenME: init resulting options %lld\n", (long long) lOptions));
		break;

	case MQXR_TERM:                                  /* Termination             */
		DBGPRINTF((OST, "YusenME: term\n"));
		DBGLOG((OSL,
			TRC_BUF_SIZE,
			"YusenME: ver=%s ChannelName=\"%.*s\" ended.\n",
			VERSION,
			rlnts(MQ_CHANNEL_NAME_LENGTH, pChannelDefinition->ChannelName),
			pChannelDefinition->ChannelName));
		/* no action */
		break;

	case MQXR_MSG:                                   /* Message                 */
													 /* NOTE: pExitBufferAddr is used to pass flags between exit points      */
		lOptions = (MQINT64)*pExitBufferAddr;
		DBGPRINTF((OST, "YusenME: msg MsgType=%d Report=0x%08x\n",
			pXqh->MsgDesc.MsgType,
			pXqh->MsgDesc.Report));

		if ((pChannelDefinition->ChannelType == MQCHT_RECEIVER
			|| pChannelDefinition->ChannelType == MQCHT_REQUESTER))
		{

			/* check if incoming msg is asking for exception/expiration report  */
			if ((lOptions & MQME_EXP)
				&& (pXqh->MsgDesc.Report & (MQRO_EXCEPTION
					| MQRO_EXCEPTION_WITH_DATA
					| MQRO_EXCEPTION_WITH_FULL_DATA
					| MQRO_EXPIRATION
					| MQRO_EXPIRATION_WITH_DATA
					| MQRO_EXPIRATION_WITH_FULL_DATA)))
			{
				/* And if so remove the EXCEPTION/EXPIRATION options            */
				pXqh->MsgDesc.Report = pXqh->MsgDesc.Report
					& (~MQRO_EXCEPTION
						& ~MQRO_EXCEPTION_WITH_DATA
						& ~MQRO_EXCEPTION_WITH_FULL_DATA
						& ~MQRO_EXPIRATION
						& ~MQRO_EXPIRATION_WITH_DATA
						& ~MQRO_EXPIRATION_WITH_FULL_DATA);
				DBGPRINTF((OST,
					"YusenME: Unwanted Report options were removed (EXC/EXP).\n"));
			}

			/* check if incoming msg is asking for coa/cod report               */
			if ((lOptions & MQME_COA)
				&& (pXqh->MsgDesc.Report & (MQRO_COA
					| MQRO_COA_WITH_DATA
					| MQRO_COA_WITH_FULL_DATA
					| MQRO_COD
					| MQRO_COD_WITH_DATA
					| MQRO_COD_WITH_FULL_DATA)))
			{
				/* And if so remove the COA/COD options                         */
				pXqh->MsgDesc.Report = pXqh->MsgDesc.Report
					& (~MQRO_COA
						& ~MQRO_COA_WITH_DATA
						& ~MQRO_COA_WITH_FULL_DATA
						& ~MQRO_COD
						& ~MQRO_COD_WITH_DATA
						& ~MQRO_COD_WITH_FULL_DATA);
				DBGPRINTF((OST,
					"YusenME: Unwanted Report options were removed (coa/cod).\n"));
			}

			/* check if incoming msg is ask us to discard the message in case   */
			/* of problems                                                      */
			if ((lOptions & MQME_DISC)
				&& (pXqh->MsgDesc.Report & (MQRO_DISCARD_MSG
					| MQRO_PASS_DISCARD_AND_EXPIRY)))
			{
				/* And if so remove the discard options */
				pXqh->MsgDesc.Report = pXqh->MsgDesc.Report
					& (~MQRO_DISCARD_MSG
						& ~MQRO_PASS_DISCARD_AND_EXPIRY);
				DBGPRINTF((OST,
					"YusenME: Unwanted Report options were removed (discard).\n"));
			}

			/* check if incoming msg enforce MCAUSER                            */
			if (lOptions & MQME_MCA)
			{
				memset(pXqh->MsgDesc.UserIdentifier, '\00', MQ_USER_ID_LENGTH);
				memcpy(pXqh->MsgDesc.UserIdentifier,
					pChannelDefinition->MCAUserIdentifier,
					MQ_USER_ID_LENGTH);
				DBGPRINTF((OST, "YusenME: Replaced UserIdentifier with \"%.*s\"\n",
					MQ_USER_ID_LENGTH, pXqh->MsgDesc.UserIdentifier));

				memset(pXqh->MsgDesc.AccountingToken, '\00', MQ_ACCOUNTING_TOKEN_LENGTH);
				DBGPRINTF((OST, "YusenME: Replaced AccountingToken with null.\n"));
			}
		}

		/* Modifying the ReplyToQmgr and ReplyToQ on normal channels to allow   */
		/* a flexible B2B integration without revealing your bussiness partners */
		/* when modifying your WebSphere MQ infrastructure.                     */
		/* Only change if ReplyToQ is in use                                    */
		if ((pChannelDefinition->ChannelType == MQCHT_RECEIVER
			|| pChannelDefinition->ChannelType == MQCHT_REQUESTER
			|| pChannelDefinition->ChannelType == MQCHT_SENDER
			|| pChannelDefinition->ChannelType == MQCHT_SERVER)
			&& (pXqh->MsgDesc.ReplyToQ[0]>' ')
			&& (lOptions & MQME_RTQM))
		{


			/* prepare for changing ReplyToQmgr and ReplyToQ */
			memset(cExitDataWork, '\00', sizeof(cExitDataWork));
			memcpy(cExitDataWork, pChannelDefinition->MsgUserData,
				MQ_EXIT_DATA_LENGTH);

			/* point to new ReplyToQmgr from MsgUserData                        */
			p = strstr(cExitDataWork, "RTQM=");
			if (p != NULL)
			{
				p = p + 5;
				/* Obtain the queue manager name                                */
				memset(cWorkRQmgr, '\00', sizeof(cWorkRQmgr));
				memset(cWorkRQmgr1, '\00', sizeof(cWorkRQmgr1));

				u = (char*)strtok_s(p, "/", &strtok_cb);
				if (u != NULL)  /* Should allways be there                       */
				{
					memcpy(cWorkRQmgr, p, rlnts(MQ_EXIT_DATA_LENGTH, p));
					memcpy(cWorkRQmgr1, pXqh->MsgDesc.ReplyToQMgr,
						rlnts(MQ_Q_MGR_NAME_LENGTH, pXqh->MsgDesc.ReplyToQMgr));
				}

				if (strcmp(cWorkRQmgr, cWorkRQmgr1))
				{
					DBGPRINTF((OST, "YusenME: Replaced WQmgr \"%s\" Qmgr1\"%s\"\n",
						cWorkRQmgr, cWorkRQmgr1));

					/* Build up the new ReplyToQueue                            */
					memset(cWorkReplyQueue, '\00', sizeof(cWorkReplyQueue));
					strncpy_s(cWorkReplyQueue, sizeof(cWorkReplyQueue), pXqh->MsgDesc.ReplyToQMgr,
						rlnts(MQ_Q_MGR_NAME_LENGTH,
							pXqh->MsgDesc.ReplyToQMgr));
					strcat_s(cWorkReplyQueue, sizeof(cWorkReplyQueue), ".");
					strncat_s(cWorkReplyQueue, MQ_Q_NAME_LENGTH, pXqh->MsgDesc.ReplyToQ,
						rlnts(MQ_Q_NAME_LENGTH, pXqh->MsgDesc.ReplyToQ));

					DBGPRINTF((OST, "YusenME: Old ReplyToQ \"%.*s\"\n",
						rlnts(MQ_Q_MGR_NAME_LENGTH,
							pXqh->MsgDesc.ReplyToQ),
						pXqh->MsgDesc.ReplyToQ));

					DBGPRINTF((OST, "YusenME: new ReplyToQ \"%s\"\n",
						cWorkReplyQueue));

					/*Adjust the ReplyToQ, just take MQ_Q_NAME_LENGTH           */
					strncpy_s(pXqh->MsgDesc.ReplyToQ, MQ_Q_NAME_LENGTH, cWorkReplyQueue,
						rlnts(MQ_Q_NAME_LENGTH, cWorkReplyQueue));

					/* And now change the replyToQMgr                           */
					memset(pXqh->MsgDesc.ReplyToQMgr, '\00',
						MQ_Q_MGR_NAME_LENGTH);
					memcpy(pXqh->MsgDesc.ReplyToQMgr, cWorkRQmgr,
						rlnts(MQ_Q_MGR_NAME_LENGTH, cWorkRQmgr));

					DBGPRINTF((OST,
						"YusenME: Replaced ReplyToQMgr with \"%.*s\"\n",
						rlnts(MQ_Q_MGR_NAME_LENGTH,
							pXqh->MsgDesc.ReplyToQMgr),
						pXqh->MsgDesc.ReplyToQMgr));
					DBGPRINTF((OST,
						"YusenME: Replaced ReplyToQueue with \"%.*s\"\n",
						rlnts(MQ_Q_NAME_LENGTH, pXqh->MsgDesc.ReplyToQ),
						pXqh->MsgDesc.ReplyToQ));
				}
			}
		}

		break;                                       /* switch - Message        */

	default:                                         /* unsupported exit reason */
		DBGPRINTF((OST, "YusenME: error - unsupported exit reason\n"));
		/* prevent msg going any further */
		pChannelExitParams->ExitResponse = MQXCC_SUPPRESS_FUNCTION;
		break;

	}                                                /* switch - ExitReason     */

	DBGPRINTF((OST, "YusenME: exit ExitResponse=%s (%ld)\n\n",
		MQExitResponseType(pChannelExitParams),
		pChannelExitParams->ExitResponse));

	return;

}                                                    /* MsgExit                 */
/********************************************************************************/
/*                              end                                             */
/********************************************************************************/

